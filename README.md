datalife_timesheet_work_price
=============================

The timesheet_work_price module of the Tryton application platform.

[![Build Status](https://drone.io/gitlab.com/datalifeit/trytond-timesheet_work_price/status.png)](https://drone.io/gitlab.com/datalifeit/trytond-timesheet_work_price/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
