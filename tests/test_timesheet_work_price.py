# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import datetime
from decimal import Decimal
import unittest
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.modules.company.tests import create_company, set_company


class TimesheetWorkPriceTestCase(ModuleTestCase):
    """Test Timesheet Work Price module"""
    module = 'timesheet_work_price'

    @with_transaction()
    def test_compute_cost_price(self):
        'Test compute_cost_price'
        pool = Pool()
        Party = pool.get('party.party')
        Work = pool.get('timesheet.work')
        WorkCostPrice = pool.get('timesheet.work_cost_price')

        cost_prices = [
            (datetime.date(2011, 1, 1), Decimal(10)),
            (datetime.date(2012, 1, 1), Decimal(15)),
            (datetime.date(2013, 1, 1), Decimal(20)),
            ]
        test_prices = [
            (datetime.date(2010, 1, 1), 0),
            (datetime.date(2011, 1, 1), Decimal(10)),
            (datetime.date(2011, 6, 1), Decimal(10)),
            (datetime.date(2012, 1, 1), Decimal(15)),
            (datetime.date(2012, 6, 1), Decimal(15)),
            (datetime.date(2013, 1, 1), Decimal(20)),
            (datetime.date(2013, 6, 1), Decimal(20)),
            ]
        party = Party(name='Pam Beesly')
        party.save()
        company = create_company()
        with set_company(company):
            work = Work(name='Work 1', company=company)
            work.save()
            for date, cost_price in cost_prices:
                WorkCostPrice(
                    work=work,
                    date=date,
                    cost_price=cost_price).save()
            for date, cost_price in test_prices:
                self.assertEqual(work.compute_cost_price(date), cost_price)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TimesheetWorkPriceTestCase))
    return suite
